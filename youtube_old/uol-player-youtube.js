(function(window){
'use-strict'
function YoutubeApi(config) {
		
        var self = this;
        self.isPaused = false;
		var config = config || {};
		var debug = config.debug;
        var player;
        var updating = false;
        var currentTrackValue;
        var delayToUpdate;
        var mute = false;
        var prefixLog = '[UOL RICHMEDIA TRACKING]';

        function onPlayerReady(e) {
            updateTime();
        }

        function onPlayerStateChange(e) {
            if (e.data === YT.PlayerState.PLAYING) {
                updating = false;
                updateTime();
                if(self.isPaused === false) {
                    trackByState('play');
                } else {
                    trackByState('resumed');
                }
            } else if(e.data === YT.PlayerState.PAUSED) {
                updating = true;
                self.isPaused = true;
                trackByState('paused');
            }
        }

        function onPlayerPlaybackQualityChange(e) {
            if (debug.toString() === 'true') console.log('[QUALITY STATUS]', e);
        }

        function onPlayerError(e) {
            if (debug.toString() === 'true') console.log('[ERRO AO CARREGAR VÍDEO]', e);
        }

        function trackByState(value) {
            if (currentTrackValue === undefined || currentTrackValue !== value) {
                trackByParams(value, config.trackingValues[value]);
                currentTrackValue = value;
            }
        }
        
        function trackByParams (params, id) {
            var url, path;
            var timestamp = (new Date()).getTime();
            var imgTracker = new Image();
            path = "http://www5.smartadserver.com/imp?imgid=";

            if (id === "" || id === undefined) {
              return;
            }

            url = path+id+"&tmstp="+timestamp+"&tgt=&"+params;

            if(debug.toString() === 'true') {
                logMessage('TRACKING', url);
            } else {
                imgTracker.src = url;
            }
        }

        function logMessage(type, msg) {
            console.log('[UOL RICHMEDIA '+type+']', msg);
        }

        function formatTime(time){
            var time = Math.round(time);
            var minutes = Math.floor(time / 60),
            seconds = time - minutes * 60;
            seconds = seconds < 10 ? '0' + seconds : seconds;
            return minutes + ":" + seconds;
        }

        function updateTime() {
            var duration = player.getDuration();
            var sliceTime = [
                formatTime(duration*0.25),
                formatTime(duration*0.5),
                formatTime(duration*0.75),
                formatTime(duration)
            ];
            var flagInterval = setInterval(function() {
                var currentTime = formatTime(player.getCurrentTime());
                for (var i = 0; i < sliceTime.length; i++) {
                    if(currentTime === sliceTime[i]) {
                        if(i === 0) trackByState('firstQuartile');
                        if(i === 1) trackByState('midpoint');
                        if(i === 2) trackByState('thirdQuartile');
                        if(i === 3) {
                            trackByState('complete');
                            updating = true;
                            self.isPaused = false;
                        }
                    }
                };
                
                if (!mute && checkIsMuted() === 'mute') {
                    trackByState('mute');
                    mute = true;
                }
                
                if (mute && checkIsMuted() === 'unmute') {
                    trackByState('unmute');
                    mute = false;
                }

                if(updating === true) {
                    clearInterval(flagInterval);
                }
            }, delayToUpdate);
        }

        function appendScript() {
            var tag = document.createElement('script');
            tag.setAttribute('src','https://www.youtube.com/iframe_api');
            var scriptTag = document.getElementsByTagName('script')[0];
            scriptTag.parentNode.insertBefore(tag, scriptTag);
        }

        function checkIsMuted() {
            return ((player.isMuted()) ? 'mute' : 'unmute');
        }
        
        self.setQuality = function(value){
            if(player) player.setPlaybackQuality(value); //Ex. hd720
        }

        self.stop = function() {
            if(player) player.stopVideo();
        };

        self.play = function() {
            if(player) player.playVideo();
        };

        self.mute = function() {
            if(player) player.mute();
        };

        self.unmute = function() {
            if(player) player.unMute();
        };

        self.createPlayer = function() {
            player = new YT.Player(config.containerId.toString(), {
                videoId: config.videoId,
                playerVars: { 
                    'autohide': ((config.autohide !== undefined && config.autohide.toString() == 'true') ? 1 : 0),
                    'autoplay': ((config.autoplay !== undefined && config.autoplay.toString() == 'true') ? 1 : 0),
                    'controls': ((config.controls !== undefined && config.controls) ? config.controls : 1),
                    'showinfo': ((config.showinfo !== undefined && config.showinfo === 'true') ? 1 : 0),
                    'playsinline': ((config.playsinline !== undefined && config.fullscreen === 'true') ? 1 : 0),
                    'modestbranding' : ((config.modestbranding !== undefined && config.modestbranding === 'true') ? 1 : 0),
                    'fs': ((config.fullscreen !== undefined && config.fullscreen.toString() === 'true') ? 1 : 0),
                    'rel': ((config.rel !== undefined && config.rel === 'true') ? 1 : 0)
                  },
                events: {
                    'onReady': onPlayerReady,
                    'onStateChange': onPlayerStateChange,
                    'onPlaybackQualityChange': onPlayerPlaybackQualityChange,
                    'onError': onPlayerError
                }
            });
        };

        self.init = function() {
            appendScript();
        };

        self.render = function (){
            self.createPlayer();
        };
    }
	
	function UOLPlayerYoutube(name, config) {
		window.UOLI = window.UOLI || {};
    	window.UOLI[name] = {};
    	window.UOLI[name].youtube = new YoutubeApi(config);
    	window.UOLI[name].youtube.init();
    	window.onYouTubeIframeAPIReady = window.UOLI[name].youtube.render;
        return window.UOLI[name].youtube;
	}
	
	window.UOLPlayerYoutube = UOLPlayerYoutube;
})(window);
