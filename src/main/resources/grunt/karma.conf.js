module.exports = function (config) {
    config.set({
        frameworks: ['jasmine'],
        plugins: [
            'karma-jasmine',
            'karma-jasmine-html-reporter',
            'karma-junit-reporter',
            'karma-chrome-launcher',
            'karma-phantomjs-launcher'
        ],
        files: [
            '../../../test/resources/**/*.js'
        ],
        reporters: ['progress', 'junit'],
        junitReporter: {
            outputFile: 'test-results.xml'
        },
        port: 8080,
        autoWatch: true,
        background: true,
        exclude: [],
        colors: true,
        browserNoActivityTimeout: 60000,
        browsers: [
            'PhantomJS'
        ],
        singleRun: true,
        hostname: 'karma.tm.uol.com.br'
    });
};
