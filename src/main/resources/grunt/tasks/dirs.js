module.exports = {
    src: '../../js',
    css: '../../css',
    htmlSrc: '../../html',
    gruntFiles: '/',
    build: '../build',
    test: '../../../test/js'
};