module.exports = function (grunt) {
    var dirs = require('./dirs');

    return {
        options: {
            banner: '(function (window, document, undefined){\n\'use strict\';\n',
            footer: '\n})(this, document);'
        },
        youtube: {
            src: [
                '<%= dirs.src %>/uolpd/youtube/Tracking.js',
                '<%= dirs.src %>/uolpd/youtube/YoutubeApi.js',
                '<%= dirs.src %>/uolpd/youtube/VideoPlayer.js',
                '<%= dirs.src %>/uolpd/youtube/init.js'
            ],
            dest: '<%= dirs.build %>/uol-player-youtube.js'
        }
    };
};
