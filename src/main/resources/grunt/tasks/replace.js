module.exports = function (grunt) {
    var dirs = require('./dirs');
    return {
      dist: {
        options: {
          patterns: [
            {
                match: /%%TITLE%%/g,
                replacement: 'mosaico'
            },
            {
                match: /'%%WIDTH_INITIAL%%'/g,
                replacement: '300'
            },
            {
                match: /'%%HEIGHT_INITIAL%%'/g,
                replacement: '250'
            },
            {
                match: /'%%CLICKAREA_INITIAL%%'/g,
                replacement: 'true'
            },
            {
                match: '%%BACKGROUND_INITIAL%%',
                replacement: '#ffffff'
            },
            {
                match: '%%SOURCE_INITIAL%%',
                replacement: 'http://insercao.uol.com.br/teste/richmedia/2015/default/fechado/'
            },
            {
                match: '%%SOURCE_EXPANDED%%',
                replacement: 'http://insercao.uol.com.br/teste/richmedia/2015/default/expandido/'
            },
            {
                match: /'%%WIDTH_EXPANDED%%'/g,
                replacement: '955'
            },
            {
                match: /'%%HEIGHT_EXPANDED%%'/g,
                replacement: '500'
            },
            {
                match: /'%%CLICKAREA_EXPANDED%%'/g,
                replacement: 'true'
            },
            {
                match: '%%BACKGROUND_EXPANDED%%',
                replacement: '#ffffff'
            },
            {
                match: /'%%MARGIN_X%%'/g,
                replacement: '0'
            },
            {
                match: /'%%MARGIN_Y%%'/g,
                replacement: '0'
            },
            {
                match: '%%ALIGN%%',
                replacement: 'TOP_LEFT'
            },
            {
                match: '%%BANNER_ID%%',
                replacement: 'banner-300x250-5'
            }
          ]
        },
        files: [
            {
                expand: true, 
                flatten: true, 
                src: ['<%= dirs.build %>/template-mosaico.js'], 
                dest: '<%= dirs.build %>/'
            }
        ]
      }
    }
}