function VideoPlayer(name, config) {
    window.UOLI = window.UOLI || {};
    window.UOLI[name] = {};
    window.UOLI[name].youtube = new YoutubeApi(config);
    window.UOLI[name].youtube.init();
    window.onYouTubeIframeAPIReady = window.UOLI[name].youtube.render;
    return window.UOLI[name].youtube;
}