function Tracking(debug) {
    
    var $public = this;
	var $private = {};
    $private.PATH = "http://www5.smartadserver.com/imp?imgid=";
    
    $public.trackByParams = function(params, id) {
        var timestamp = (new Date()).getTime();
        var imgTracker = new Image();
        
        if (id === "" || id === undefined) {
          return;
        }
        
        $private.url = $private.PATH+id+"&tmstp="+timestamp+"&tgt=&"+params;
		
		if(debug.toString() === 'true') {
            $private.logMessage('TRACKING', $private.url);
        } else {
            imgTracker.src = $private.url;
        }
    };
	
    $private.logMessage = function(type, msg) {
        console.log('[UOL RICHMEDIA '+type+']', msg);
    };
}