function YoutubeApi(config) {
    
    var $public = this;
    var $private = {};

    $public.isPaused = false;
    
    var debug = config.debug;
    var player;
    var updating = false;
    var currentTrackValue;
    var delayToUpdate;
    var mute = false;

    var prefixLog = '[UOL RICHMEDIA TRACKING]';
    var PATH_YOUTUBE_API = 'https://www.youtube.com/iframe_api';
    var tracking = new Tracking(debug);

    $private.onPlayerReady = function(e) {
        $private.updateTime();
    };

    $private.onPlayerStateChange = function(e) {
        if (e.data === YT.PlayerState.PLAYING) {
            updating = false;
            $private.updateTime();
            if($public.isPaused === false) {
                $private.trackByState('play');
            } else {
                $private.trackByState('resumed');
            }
        } else if(e.data === YT.PlayerState.PAUSED) {
            updating = true;
            $public.isPaused = true;
            $private.trackByState('paused');
        }
    };

    $private.onPlayerPlaybackQualityChange = function(e) {
        if (debug.toString() === 'true') console.log('[QUALITY STATUS]', e);
    };

    $private.onPlayerError = function(e) {
        if (debug.toString() === 'true') console.log('[ERRO AO CARREGAR VÍDEO]', e);
    };

    $private.trackByState = function(value) {
        if (currentTrackValue === undefined || currentTrackValue !== value) {
            tracking.trackByParams(value, config.trackingValues[value]);
            currentTrackValue = value;
        }
    };
    
    $private.formatTime = function(time){
        var timeRound = Math.round(time);
        var minutes = Math.floor(timeRound / 60),
        seconds = time - minutes * 60;
        seconds = seconds < 10 ? '0' + seconds : seconds;
        return minutes + ":" + seconds;
    };

    $private.updateTime = function() {
        var duration = player.getDuration();
        var sliceTime = [
            $private.formatTime(duration*0.25),
            $private.formatTime(duration*0.5),
            $private.formatTime(duration*0.75),
            $private.formatTime(duration)
        ];
        var flagInterval = setInterval(function() {
            var currentTime = $private.formatTime(player.getCurrentTime());
            for (var i = 0; i < sliceTime.length; i++) {
                if(currentTime === sliceTime[i]) {
                    if(i === 0) $private.trackByState('firstQuartile');
                    if(i === 1) $private.trackByState('midpoint');
                    if(i === 2) $private.trackByState('thirdQuartile');
                    if(i === 3) {
                        $private.trackByState('complete');
                        updating = true;
                        $public.isPaused = false;
                    }
                }
            }
            
            if (!mute && $private.checkIsMuted() === 'mute') {
                $private.trackByState('mute');
                mute = true;
            }
            
            if (mute && $private.checkIsMuted() === 'unmute') {
                $private.trackByState('unmute');
                mute = false;
            }

            if(updating === true) {
                clearInterval(flagInterval);
            }
        }, delayToUpdate);
    };

    $private.appendScript = function() {
        var tag = document.createElement('script');
        tag.setAttribute('src', PATH_YOUTUBE_API);
        var scriptTag = document.getElementsByTagName('script')[0];
        scriptTag.parentNode.insertBefore(tag, scriptTag);
    };

    $private.checkIsMuted = function() {
        return ((player.isMuted()) ? 'mute' : 'unmute');
    };
    
    $public.setQuality = function(value){
        if(player) player.setPlaybackQuality(value); //Ex. hd720
    };

    $public.stop = function() {
        if(player) player.stopVideo();
    };

    $public.play = function() {
        if(player) player.playVideo();
    };

    $public.mute = function() {
        if(player) player.mute();
    };

    $public.unmute = function() {
        if(player) player.unMute();
    };

    $public.createPlayer = function() {
        player = new YT.Player(config.containerId.toString(), {
            videoId: config.videoId,
            playerVars: { 
                'autohide': ((config.autohide !== undefined && config.autohide.toString() == 'true') ? 1 : 0),
                'autoplay': ((config.autoplay !== undefined && config.autoplay.toString() == 'true') ? 1 : 0),
                'controls': ((config.controls !== undefined && config.controls) ? config.controls : 1),
                'showinfo': ((config.showinfo !== undefined && config.showinfo === 'true') ? 1 : 0),
                'playsinline': ((config.playsinline !== undefined && config.fullscreen === 'true') ? 1 : 0),
                'modestbranding' : ((config.modestbranding !== undefined && config.modestbranding === 'true') ? 1 : 0),
                'fs': ((config.fullscreen !== undefined && config.fullscreen.toString() === 'true') ? 1 : 0),
                'rel': ((config.rel !== undefined && config.rel === 'true') ? 1 : 0)
              },
            events: {
                'onReady': $private.onPlayerReady,
                'onStateChange': $private.onPlayerStateChange,
                'onPlaybackQualityChange': $private.onPlayerPlaybackQualityChange,
                'onError': $private.onPlayerError
            }
        });
    };

    $public.init = function() {
        $private.appendScript();
    };

    $public.render = function (){
        $public.createPlayer();
    };
}