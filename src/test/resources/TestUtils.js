var TestUtils = {
    createElement: function (elementType) {
        return document.createElement(elementType);
    },

    setElementAttributes: function (element, attributes) {
        for (var attr in attributes) {
            element.setAttribute(attr, attributes[attr]);
        }
    },

    setDOMElement: function (element) {
        document.body.appendChild(element);
    },

    removeDOMElement: function (element) {
        if (element) {
            element.parentNode.removeChild(element);
        }
    },

    waitDocumentReady: function (fn) {
        var interval = window.setInterval(function () {
            if (document.readyState && document.readyState === 'complete') {
                window.clearInterval(interval);
                fn();
            }
        }, 50);
    },

    wait: function (fn, timeout) {
        var self = this;
        window.setTimeout(function () {
            fn(self);
        }, timeout);
    },

    waitElementPresent: function (selector, fn) {
        var timer = 0;
        var elementInterval = window.setInterval(function() {
            timer += 5;

            var element = document.querySelector(selector);
            if (element) {
                window.clearInterval(elementInterval);
                fn(element);
            }

            if (timer >= 10000) {
                window.clearInterval(elementInterval);
                throw new Error('Não foi encontrado o elemento '+ selector +' durante um intervalo de 10 segundos');
            }
        }, 5);
    },
    getExpirationDate: function (expireDays) {
        var exdate = exdate || new Date();
        exdate.setDate(exdate.getDate() + expireDays);
        exdate.setHours(23);
        exdate.setMinutes(59);
        exdate.setSeconds(59);
        return exdate.toUTCString();
    },
    createCookieString: function (name, value, expireDays, domain) {
        var cookie = name + "=" + value + ";expires='" + TestUtils.getExpirationDate(expireDays) + "' GMT;path=/;";
        if (domain) {
            cookie += "domain="+ domain +";";
        }
        return cookie;
    },
    deleteCookie: function (name, domain) {
        document.cookie = TestUtils.createCookieString(name, '', -100, domain);
    },
    clearCookies : function () {
        var cookies = document.cookie.split(";");
        for (var i = 0; i < cookies.length; i++) {
            TestUtils.deleteCookie(cookies[i].split("=")[0]);
        }
    },
    getCookies: function (cookieName) {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++){
            if (cookies[i].indexOf(cookieName + '=') != -1) {
                return cookies[i].split('=')[1];
            }
        }
        return;
    }
};
