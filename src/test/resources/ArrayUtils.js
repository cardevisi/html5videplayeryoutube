var ArrayUtils = {
    sortIndexNumber: function (array) {
        var multiplier = array.length - 1;
        return Math.round(Math.random() * multiplier);
    },

    removeRandomIndex: function (array) {
        var randomIndex = this.sortIndexNumber(array);
        array.splice(randomIndex, 1);
    },

    inArray: function (element, arr) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i] == element) {
                return true;
            }
        }
        return false;
    }
};